<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    
     <link rel="stylesheet" href="styles/owl.carousel.min.css">
     <link rel="stylesheet" href="styles/owl.theme.green.min.css">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">
     <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">  
    <link rel="stylesheet" type="text/css" href="styles/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
    <title>Aivaras</title>
</head>
<body>

<?php include "navigation.php" ?>

<div class="container personal-containers">
    <h2>About Me</h2>
      <div class="personal-container">
          <div class="personal-image">
              <img src="https://vignette.wikia.nocookie.net/teamfourstar/images/a/a8/Krillin_Sanchez.png/revision/latest?cb=20160405220144" alt="">
          </div><!--
              
             --><div class="Personal-information">

                <h3>About Me</h3>
                <p>Lorizzle ipsizzle dolor mah nizzle amet, brizzle adipiscing elit. Nullam velizzle, break it down volutpizzle, suscipit gizzle, funky fresh vizzle, owned. Pellentesque boofron tortor. Sizzle owned. Fizzle you son of a bizzle dolizzle dapibus turpis fo shizzle my nizzle boofron. 
                <h3>My Hobies</h3>
                <p>Maurizzle dawg nibh pot turpizzle. Vestibulum izzle fo shizzle. Pellentesque i saw beyonces tizzles and my pizzle went crizzle rhoncus i'm in the shizzle. In hac habitasse platea dictumst. Ass dapibizzle. Curabitur tellizzle bizzle, pretizzle crazy, boofron izzle, cool vitae, nunc. Crunk check it out. Integer semper velit sizzle dang.</p>
         
      </div>
    </div>
</div>
     
     
     <div class="contacts">
			<h2 class="contact-header">Contact Me</h2>
				<div class="container contact-container">
					<div class="form-container contact-div">
						<form class="forma" action="contactform.php" method="post">
							<input class="input" type="text" name="name" placeholder="Jūsų vardas..."><br>
							<input class="input" type="text" name="mail" placeholder="Jūsų elektroninis paštas..."><br>
							<input class="input" type="text" name="subject" placeholder="Tema..."><br>
							<textarea class="text-area" name="message" placeholder="Jūsų klausimas..."></textarea><br>
                            <button class="input submit" type="submit" name="submit" >Siusli laiska</button>
						</form>
					</div>
                    <!---->
					<div class="map-container contact-div">
						<iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2304.934820442126!2d25.291859215890355!3d54.71076988028759!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46dd96b1c1c3bc9f%3A0xb4eca31b162c5579!2sVilnius+Coding+School!5e0!3m2!1slt!2slt!4v1534936213891" frameborder="0" style="border:0" allowfullscreen></iframe>
						<p>Vilniaus g. 25, Vilnius 08236 <br> GunShop.lt 
							(8-666) 66666
						</p>
					</div>
				</div>
		</div>	
      
<?php include "footer.php" ?>


<script src="scripts/owl.carousel.min.js"></script>        
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script> 
<script  type="text/javascript" src="scripts/script.js"></script>    
</body>
</html>






