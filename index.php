<?php include "database_connection.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    
     <link rel="stylesheet" href="styles/owl.carousel.min.css">
     <link rel="stylesheet" href="styles/owl.theme.green.min.css">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">
     <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">  
    <link rel="stylesheet" type="text/css" href="styles/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
    <title>Pagrindinis puslapis</title>
</head>
<body>

<?php include "navigation.php" ?>

<!--carouser start  -->

 <div class="slider">
    <ul class="slides">
      <li>
        <img src="https://static.wixstatic.com/media/d03a2c_367c4af6f87e451da6ac74ec7d2446f8~mv2.jpg_srz_1349_609_85_22_0.50_1.20_0.00_jpg_srz"> <!-- random image -->
        <div class="caption center-align">
          <h3>HANDGUNS!</h3>
          <h5 class="light grey-text text-lighten-3">All You Need!</h5>
        </div>
      </li>
      <li>
        <img src="https://fair.it/images/prodotti/da-caccia/dacaccia.jpg"> <!-- random image -->
        <div class="caption left-align">
          <h3>SHOTGUNS!</h3>
          <h5 class="light grey-text text-lighten-3">All Types.</h5>
        </div>
      </li>
      <li>
        <img src="http://p-gruporpp-media.s3.amazonaws.com/2016/06/13/166637qewqdasdsajpg.jpg"> <!-- random image -->
        <div class="caption right-align">
          <h3>RIFLES!</h3>
          <h5 class="light grey-text text-lighten-3">For All Kinds Of Temperament.</h5>
        </div>
      </li>
      <li>
        <img src="https://i.ytimg.com/vi/VDNpLqnoJU8/maxresdefault.jpg"> <!-- random image -->
        <div class="caption center-align">
          <h3>PLUSH TOYS!</h3>
          <h5 class="light grey-text text-lighten-3">Best Sellers!</h5>
        </div>
      </li>
    </ul>
  </div>
  
<!--carouser ends  -->  
 
 <!--guns and toy section starts -->  
  
<div class="row">
      <div class="col s12 center"><h2>BEST SELLER!</h2></div>
</div>
  
  
  
<?php
    
    $sql = "SELECT title, price, image, id FROM goods WHERE category = 'guns' LIMIT 4";

	$result = mysqli_query($conn, $sql);


	if (mysqli_num_rows($result) > 0) {
        
echo  '<div class="container">';      
echo  '<div class="row index-guns">';

	    while($row = mysqli_fetch_assoc($result)) {

        $id = $row["id"];
	    $price = $row["price"]; 
	    $image = $row["image"]; 
	    $title = $row["title"]; 
?>
    
   
 
      <div class="col s12 m6 l3 center">
          <h5><?php echo $title; ?></h5>
          <?php echo $image; ?>
          <h4><?php echo "$" . $price; ?></h4>
      </div> 
 
<!--
      <div class="col s8 offset-s2 m5 l2 center"><p>TAURUS G2C 9MM</p><img src="https://assets.academy.com/mgen/38/20045538.jpg?is=500,500">
      <h4></h4></div> 
-->
      
      <?php  } echo '</div>'; echo '</div>'; }  ?>
      
      
<!--
      <div class="col s8 offset-s2 m5 offset-m1 l2 l2 center"><p>Ruger LC9S 9MM 3.12 7RD BL</p><img src="https://cdn.shopify.com/s/files/1/1083/0266/products/1911-soap-gun-isolated_large.jpeg?v=1449510574"><h4>$249.00</h4></div>
      
      <div class="col s8 offset-s2 m5 l2 center"><p>S&W M&P9 Shield 7+1/8+1</p><img src="https://s7d2.scene7.com/is/image/academy/20162781?wid=500&hei=500"><h4>$319.00</h4>
      </div>
    </div>
-->
   
   
    
<div class="row">
      <div class="col s12 center blue"><h2>BEST SELLER!</h2></div>
</div>    
   
   <?php
    
    $sql = "SELECT title, price, image, id FROM goods WHERE category = 'toys' LIMIT 4";
    
    $result = mysqli_query($conn, $sql);
    
if (mysqli_num_rows($result) > 0) {
        
echo  '<div class="container">';      
echo  '<div class="row index-guns">';

	    while($row = mysqli_fetch_assoc($result)) {

        $id = $row["id"];
	    $price = $row["price"]; 
	    $image = $row["image"]; 
	    $title = $row["title"]; 
?>
    
   
 
      <div class="col s12 m6 l3 center">
          <h5><?php echo $title; ?></h5>
          <?php echo $image; ?>
          <h4><?php echo "$" . $price; ?></h4>
      </div> 
 
<!--
      <div class="col s8 offset-s2 m5 l2 center"><p>TAURUS G2C 9MM</p><img src="https://assets.academy.com/mgen/38/20045538.jpg?is=500,500">
      <h4></h4></div> 
-->
      
      <?php  } echo '</div>'; echo '</div>'; }  ?>
     
     
 <!--guns and toys section ends -->  
   
   <div class="row">
      <div class="col s12 center blue"><h2>TOP!</h2></div>
</div>  
    
<!--carousel section starts -->  


<div class="owl-carousel owl-theme aivaro-carusele">
    <div class="item"><h4><img src="https://assets.academy.com/mgen/12/20039512.jpg?is=500,500"></h4></div>
    <div class="item"><h4><img src="https://sc01.alicdn.com/kf/HTB1ehp1SFXXXXarXXXXq6xXFXXX6/Happy-Tree-Friends-NUTTY-Large-Plush-New.jpg_350x350.jpg"></h4></div>
    <div class="item"><h4><img src="https://assets.academy.com/mgen/38/20045538.jpg?is=500,500"></h4></div>
    <div class="item"><h4><img src="http://www.strapya-world.com/images/medium/149/149-163536_MED.jpg"></h4></div>
    <div class="item"><h4><img src="https://cdn.shopify.com/s/files/1/1083/0266/products/1911-soap-gun-isolated_large.jpeg?v=1449510574"></h4></div>
    <div class="item"><h4><img src="https://images-na.ssl-images-amazon.com/images/I/31wopiauxCL.jpg"></h4></div>
    <div class="item"><h4><img src="https://s7d2.scene7.com/is/image/academy/20162781?wid=500&hei=500"></h4></div>
    <div class="item"><h4><img src="https://media.karousell.com/media/photos/products/2017/03/03/happy_tree_friends_mini_figure_world_series_1__cuddles_1488520883_5297220f.jpg"></h4></div>
</div>

     
     <!--carousel section ends -->  
      
<?php include "footer.php" ?>


<script src="scripts/owl.carousel.min.js"></script>        
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script> 
<script  type="text/javascript" src="scripts/script.js"></script>    
</body>
</html>






