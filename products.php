
<!DOCTYPE html>
<html>
<head>
	<title></title>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">

	<link rel="stylesheet" type="text/css" href="styles/style.css">
	
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
	<?php 
		include "navigation.php";
	 ?>

	 <div class="center">
		<div class="parallax-container sort">
			<div class="parallax"><img src="images/wall.jpg"></div>
		</div>
		<div class="products-toolbar">
			<h1 class=" card-panel blue accent-4"> Our products.</h1>
		
			<ul class="sort-box">
		      <li><a class="dropdown-trigger" href="#!" data-target="dropdown-sort">Sort<i class="material-icons right">arrow_drop_down</i></a></li>
	    	</ul>
			<ul id="dropdown-sort" class="dropdown-content">
				<li class="Desc"><a href="products.php?sort=Asc">Sort Asc</a></li>
				<li class="Asc"><a href="products.php?sort=Desc">Sort Desc</a></li>
				<li><a href="products.php?sort=Title">Sort Title</a></li>
				<li class="divider"></li>
			</ul>
		</div>
	</div>


	<?php 
	include "database_connection.php";
	?>

	<?php
	if(isset($_GET["sort"])){

		if ($_GET["sort"]=="Asc"){
	   		$sql = "SELECT * FROM goods ORDER BY price";
		}else if($_GET["sort"]=="Desc"){
		   $sql = "SELECT * FROM goods ORDER BY price DESC";
		}else if($_GET["sort"]=="Title"){
		   $sql = "SELECT * FROM goods ORDER BY title";			
		}
	}else{	
		$sql = "SELECT * FROM goods";
	}
	$result = mysqli_query($conn, $sql);

	if (mysqli_num_rows($result) > 0) {	

		echo '<div>';
		echo'<div  class="blue accent-2 row center products">';

	    while($row = mysqli_fetch_assoc($result)) {
        $id = $row["id"];
	    $price = $row["price"]; 
	    $image = $row["image"]; 
	    $title = $row["title"]; 
?>
    
	
 
      <div class="col s12 m6 l4">
      	<a class="product shadow-box active" href="product.php">
      		<div class="product-image white">
          <h5><?php echo $title; ?></h5>
          <?php echo $image; ?>
          <h4><?php echo "$" . $price; ?></h4>
      		</div>
      	</a>
      </div> 
      <?php  } echo '</div>'; echo '</div>'; }  ?>



				<?php 
				include "footer.php";
				?>

				<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>

				<script type="text/javascript" src="scripts/script.js"></script>
	</body>
</html>