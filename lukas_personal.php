<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">

	<link rel="stylesheet" href="styles/owl.carousel.min.css">
	<link rel="stylesheet" href="styles/owl.theme.green.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">  
	<link rel="stylesheet" type="text/css" href="styles/style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<title>Lukas</title>
</head>
<body>

	<?php include "navigation.php" ?>

	<div class="col s12 m6 l6 center personal-Lukas">
		<h1>Waddup, my name is Lukas</h1>

		<img src="https://vignette.wikia.nocookie.net/inciclopedia/images/e/eb/Radical_90s.jpg/revision/latest?cb=20140210020849">

		<h2>My life:</h2>

		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

		<img src="https://i.gifer.com/8eg.gif">

		<h2>Doesn't that sound cool? Well, There's more!</h2>

		<p>Lorizzle ipsum dolor phat amizzle, gizzle da bomb elit. Nullam own yo' velizzle, brizzle sheezy, suscipizzle quizzle, sizzle vizzle, arcu.Lorizzle ipsum dolor phat amizzle, gizzle da bomb elit. Nullam own yo' velizzle, brizzle sheezy, suscipizzle quizzle, sizzle vizzle, arcu.</p>
	</div>

	<?php include "footer.php"; ?>

</body>
</html>