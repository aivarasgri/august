-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 03, 2018 at 08:03 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gun_shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `goods`
--

CREATE TABLE `goods` (
  `id` int(11) NOT NULL,
  `image` varchar(200) NOT NULL,
  `price` float NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `properties` text NOT NULL,
  `weight` float NOT NULL,
  `category` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `goods`
--

INSERT INTO `goods` (`id`, `image`, `price`, `title`, `description`, `properties`, `weight`, `category`) VALUES
(1, '<img src=\"https://assets.academy.com/mgen/12/20039512.jpg?is=500,500\">', 269, 'USP Elite', 'H&K 704590-A5 Elite', '<table class=\"lentele\">\r\n					<thead>\r\n						<tr>\r\n							<th>Poperties</th>	\r\n							<th>Value</th>\r\n							\r\n						</tr>\r\n					</thead>\r\n					<tbody>\r\n						<tr>\r\n							<td>Type:</td>\r\n							<td>Pistoletas</td>\r\n							\r\n							\r\n						</tr>\r\n						<tr>\r\n							<td>Kalibras:</td>\r\n							<td>4,5 mm BB</td>\r\n							\r\n						</tr>\r\n						<tr>\r\n							<td>Veikimo principas:</td>\r\n							<td>CO2</td>\r\n						</tr>\r\n						<tr>\r\n							<td>Dėtuvės talpa:</td>\r\n							<td>18</td>\r\n						</tr>\r\n						<tr>\r\n							<td>Energija:</td>\r\n							<td>3 J</td>\r\n						</tr>\r\n						<tr>\r\n							<td>Bendras ilgis:</td>\r\n							<td>215 mm</td>\r\n						</tr>\r\n						<tr>\r\n							<td>Vamzdžio ilgis:</td>\r\n							<td>123 mm</td>\r\n						</tr>\r\n						<tr>\r\n							<td>Svoris:</td>\r\n							<td>686 g</td>\r\n						</tr>												\r\n					</tbody>\r\n				</table>', 686, 'guns'),
(6, '<img src=\"https://assets.academy.com/mgen/38/20045538.jpg?is=500,500\">', 209, 'TAURUS ', 'G2C 9MM', '<thead> 						<tr> 							<th>Poperties</th>	 							<th>Value</th> 							 						</tr> 					</thead> 					<tbody> 						<tr> 							<td>Type:</td> 							<td>Pistoletas</td> 							 							 						</tr> 						<tr> 							<td>Kalibras:</td> 							<td>4,8 mm BB</td> 							 						</tr> 						<tr> 							<td>Veikimo principas:</td> 							<td>CO2</td> 						</tr> 						<tr> 							<td>DÄ—tuvÄ—s talpa:</td> 							<td>20</td> 						</tr> 						<tr> 							<td>Energija:</td> 							<td>5.5 J</td> 						</tr> 						<tr> 							<td>Bendras ilgis:</td> 							<td>218 mm</td> 						</tr> 						<tr> 							<td>VamzdÅ¾io ilgis:</td> 							<td>143 mm</td> 						</tr> 						<tr> 							<td>Svoris:</td> 							<td>886 g</td> 						</tr>												 					</tbody> 				</table>', 886, 'guns'),
(7, '<img src=\"https://cdn.shopify.com/s/files/1/1083/0266/products/1911-soap-gun-isolated_large.jpeg?v=1449510574\">', 249, 'Ruger ', 'LC9S 9MM 3.12 7RD BL', '<thead> 						<tr> 							<th>Poperties</th>	 							<th>Value</th> 							 						</tr> 					</thead> 					<tbody> 						<tr> 							<td>Type:</td> 							<td>Pistoletas</td> 							 							 						</tr> 						<tr> 							<td>Kalibras:</td> 							<td>6,8 mm BB</td> 							 						</tr> 						<tr> 							<td>Veikimo principas:</td> 							<td>CO2</td> 						</tr> 						<tr> 							<td>DÄ—tuvÄ—s talpa:</td> 							<td>25</td> 						</tr> 						<tr> 							<td>Energija:</td> 							<td>2.5 J</td> 						</tr> 						<tr> 							<td>Bendras ilgis:</td> 							<td>418 mm</td> 						</tr> 						<tr> 							<td>VamzdÅ¾io ilgis:</td> 							<td>183 mm</td> 						</tr> 						<tr> 							<td>Svoris:</td> 							<td>286 g</td> 						</tr>												 					</tbody> 				</table>', 286, 'guns'),
(8, '<img src=\"https://s7d2.scene7.com/is/image/academy/20162781?wid=500&hei=500\">', 319, 'S&W', 'M&P9 Shield 7+1/8+1', '<thead> 						<tr> 							<th>Poperties</th>	 							<th>Value</th> 							 						</tr> 					</thead> 					<tbody> 						<tr> 							<td>Type:</td> 							<td>Pistoletas</td> 							 							 						</tr> 						<tr> 							<td>Kalibras:</td> 							<td>7,8 mm BB</td> 							 						</tr> 						<tr> 							<td>Veikimo principas:</td> 							<td>CO2</td> 						</tr> 						<tr> 							<td>DÄ—tuvÄ—s talpa:</td> 							<td>35</td> 						</tr> 						<tr> 							<td>Energija:</td> 							<td>4.5 J</td> 						</tr> 						<tr> 							<td>Bendras ilgis:</td> 							<td>418 mm</td> 						</tr> 						<tr> 							<td>VamzdÅ¾io ilgis:</td> 							<td>283 mm</td> 						</tr> 						<tr> 							<td>Svoris:</td> 							<td>443g</td> 						</tr>												 					</tbody> 				</table>', 500, 'guns'),
(9, '<img src=\"https://sc01.alicdn.com/kf/HTB1ehp1SFXXXXarXXXXq6xXFXXX6/Happy-Tree-Friends-NUTTY-Large-Plush-New.jpg_350x350.jpg\">', 10, 'Nutty', 'â€œNuttyâ€ Plush', '<thead> 						<tr> 							<th>Poperties</th>	 							<th>Value</th> 							 						</tr> 					</thead> 					<tbody> 						<tr> 							<td>Type:</td> 							<td>Zaislas</td> 							 							 						</tr> 						<tr> 							<td>Spalva:</td> 							<td>zalia BB</td> 							 						</tr> 						<tr> 							<td>Veikimo principas:</td> 							<td>Zaisti</td> 						</tr> 						<tr> 							<td>dydis:</td> 							<td>35cm</td> 						</tr> 						<tr> 							<td>Svoris:</td> 							<td>200g/td> 						</tr> 						<tr> 							<td>Medziaga:</td> 							<td>medvilne</td> 						</tr> 						<tr> 							<td>Gamintojas:</td> 							<td>Made in china</td> 						</tr> 						<tr> 							<td>Platintojas</td> 							<td>UAB kazkas</td> 						</tr>												 					</tbody> 				</table>', 200, 'toys'),
(10, '<img src=\"http://www.strapya-world.com/images/medium/149/149-163536_MED.jpg\">', 8, 'Cuddles', 'â€œCuddlesâ€ Plush', '<thead> 						<tr> 							<th>Poperties</th>	 							<th>Value</th> 							 						</tr> 					</thead> 					<tbody> 						<tr> 							<td>Type:</td> 							<td>Zaislas</td> 							 							 						</tr> 						<tr> 							<td>Spalva:</td> 							<td>geltona</td> 							 						</tr> 						<tr> 							<td>Veikimo principas:</td> 							<td>Zaisti</td> 						</tr> 						<tr> 							<td>dydis:</td> 							<td>42cm</td> 						</tr> 						<tr> 							<td>Svoris:</td> 							<td>240g/td> 						</tr> 						<tr> 							<td>Medziaga:</td> 							<td>medvilne</td> 						</tr> 						<tr> 							<td>Gamintojas:</td> 							<td>Made in china</td> 						</tr> 						<tr> 							<td>Platintojas</td> 							<td>UAB kazkas</td> 						</tr>												 					</tbody> 				</table>', 240, 'toys'),
(11, '<img src=\"https://images-na.ssl-images-amazon.com/images/I/31wopiauxCL.jpg\">', 15, 'Chimpuk', 'â€œChimpukâ€ Plush', '<thead> 						<tr> 							<th>Poperties</th>	 							<th>Value</th> 							 						</tr> 					</thead> 					<tbody> 						<tr> 							<td>Type:</td> 							<td>Zaislas</td> 							 							 						</tr> 						<tr> 							<td>Spalva:</td> 							<td>ruzava</td> 							 						</tr> 						<tr> 							<td>Veikimo principas:</td> 							<td>Zaisti</td> 						</tr> 						<tr> 							<td>dydis:</td> 							<td>32cm</td> 						</tr> 						<tr> 							<td>Svoris:</td> 							<td>280g/td> 						</tr> 						<tr> 							<td>Medziaga:</td> 							<td>medvilne</td> 						</tr> 						<tr> 							<td>Gamintojas:</td> 							<td>Made in china</td> 						</tr> 						<tr> 							<td>Platintojas</td> 							<td>UAB kazkas</td> 						</tr>												 					</tbody> 				</table>', 280, 'toys'),
(12, '<img src=\"https://media.karousell.com/media/photos/products/2017/03/03/happy_tree_friends_mini_figure_world_series_1__cuddles_1488520883_5297220f.jpg\">', 12, 'Soldier', 'â€œSoldierâ€ Plush', '<thead> 						<tr> 							<th>Poperties</th>	 							<th>Value</th> 							 						</tr> 					</thead> 					<tbody> 						<tr> 							<td>Type:</td> 							<td>Zaislas</td> 							 							 						</tr> 						<tr> 							<td>Spalva:</td> 							<td>geltona</td> 							 						</tr> 						<tr> 							<td>Veikimo principas:</td> 							<td>Zaisti</td> 						</tr> 						<tr> 							<td>dydis:</td> 							<td>32cm</td> 						</tr> 						<tr> 							<td>Svoris:</td> 							<td>180g/td> 						</tr> 						<tr> 							<td>Medziaga:</td> 							<td>medvilne</td> 						</tr> 						<tr> 							<td>Gamintojas:</td> 							<td>Made in china</td> 						</tr> 						<tr> 							<td>Platintojas</td> 							<td>UAB kazkas</td> 						</tr>												 					</tbody> 				</table>', 180, 'toys'),
(13, '<img src=\"https://vignette.wikia.nocookie.net/happytreefriends/images/9/9c/Nutty_toy_image.jpg/revision/latest?cb=20130317170824\">', 15, 'Nutty', 'â€œNuttyâ€ Plush', '<thead> 						<tr> 							<th>Poperties</th>	 							<th>Value</th> 							 						</tr> 					</thead> 					<tbody> 						<tr> 							<td>Type:</td> 							<td>Zaislas</td> 							 							 						</tr> 						<tr> 							<td>Spalva:</td> 							<td>geltona</td> 							 						</tr> 						<tr> 							<td>Veikimo principas:</td> 							<td>Zaisti</td> 						</tr> 						<tr> 							<td>dydis:</td> 							<td>32cm</td> 						</tr> 						<tr> 							<td>Svoris:</td> 							<td>180g/td> 						</tr> 						<tr> 							<td>Medziaga:</td> 							<td>medvilne</td> 						</tr> 						<tr> 							<td>Gamintojas:</td> 							<td>Made in china</td> 						</tr> 						<tr> 							<td>Platintojas</td> 							<td>UAB kazkas</td> 						</tr>												 					</tbody> 				</table>', 200, 'toys');

-- --------------------------------------------------------

--
-- Table structure for table `toys`
--

CREATE TABLE `toys` (
  `id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `price` double NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `properties` text NOT NULL,
  `weight` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `toys`
--

INSERT INTO `toys` (`id`, `description`, `price`, `title`, `image`, `properties`, `weight`) VALUES
(1, 'â€œNuttyâ€ Plush', 10, 'Nutty', '<img src=\"https://sc01.alicdn.com/kf/HTB1ehp1SFXXXXarXXXXq6xXFXXX6/Happy-Tree-Friends-NUTTY-Large-Plush-New.jpg_350x350.jpg\">', '<thead> 						<tr> 							<th>Poperties</th>	 							<th>Value</th> 							 						</tr> 					</thead> 					<tbody> 						<tr> 							<td>Type:</td> 							<td>Zaislas</td> 							 							 						</tr> 						<tr> 							<td>Spalva:</td> 							<td>zalia BB</td> 							 						</tr> 						<tr> 							<td>Veikimo principas:</td> 							<td>Zaisti</td> 						</tr> 						<tr> 							<td>dydis:</td> 							<td>35cm</td> 						</tr> 						<tr> 							<td>Svoris:</td> 							<td>200g/td> 						</tr> 						<tr> 							<td>Medziaga:</td> 							<td>medvilne</td> 						</tr> 						<tr> 							<td>Gamintojas:</td> 							<td>Made in china</td> 						</tr> 						<tr> 							<td>Platintojas</td> 							<td>UAB kazkas</td> 						</tr>												 					</tbody> 				</table>', 200),
(2, 'â€œCuddlesâ€ Plush', 8, 'Cuddles', '<img src=\"http://www.strapya-world.com/images/medium/149/149-163536_MED.jpg\">', '<thead> 						<tr> 							<th>Poperties</th>	 							<th>Value</th> 							 						</tr> 					</thead> 					<tbody> 						<tr> 							<td>Type:</td> 							<td>Zaislas</td> 							 							 						</tr> 						<tr> 							<td>Spalva:</td> 							<td>geltona</td> 							 						</tr> 						<tr> 							<td>Veikimo principas:</td> 							<td>Zaisti</td> 						</tr> 						<tr> 							<td>dydis:</td> 							<td>42cm</td> 						</tr> 						<tr> 							<td>Svoris:</td> 							<td>240g/td> 						</tr> 						<tr> 							<td>Medziaga:</td> 							<td>medvilne</td> 						</tr> 						<tr> 							<td>Gamintojas:</td> 							<td>Made in china</td> 						</tr> 						<tr> 							<td>Platintojas</td> 							<td>UAB kazkas</td> 						</tr>												 					</tbody> 				</table>', 240),
(3, 'â€œChimpukâ€ Plush', 15, 'Chimpuk', '<img src=\"https://images-na.ssl-images-amazon.com/images/I/31wopiauxCL.jpg\">', '<thead> 						<tr> 							<th>Poperties</th>	 							<th>Value</th> 							 						</tr> 					</thead> 					<tbody> 						<tr> 							<td>Type:</td> 							<td>Zaislas</td> 							 							 						</tr> 						<tr> 							<td>Spalva:</td> 							<td>ruzava</td> 							 						</tr> 						<tr> 							<td>Veikimo principas:</td> 							<td>Zaisti</td> 						</tr> 						<tr> 							<td>dydis:</td> 							<td>32cm</td> 						</tr> 						<tr> 							<td>Svoris:</td> 							<td>280g/td> 						</tr> 						<tr> 							<td>Medziaga:</td> 							<td>medvilne</td> 						</tr> 						<tr> 							<td>Gamintojas:</td> 							<td>Made in china</td> 						</tr> 						<tr> 							<td>Platintojas</td> 							<td>UAB kazkas</td> 						</tr>												 					</tbody> 				</table>', 280),
(4, 'â€œSoldierâ€ Plush', 12, 'Soldier', '<img src=\"https://media.karousell.com/media/photos/products/2017/03/03/happy_tree_friends_mini_figure_world_series_1__cuddles_1488520883_5297220f.jpg\">', '<thead> 						<tr> 							<th>Poperties</th>	 							<th>Value</th> 							 						</tr> 					</thead> 					<tbody> 						<tr> 							<td>Type:</td> 							<td>Zaislas</td> 							 							 						</tr> 						<tr> 							<td>Spalva:</td> 							<td>geltona</td> 							 						</tr> 						<tr> 							<td>Veikimo principas:</td> 							<td>Zaisti</td> 						</tr> 						<tr> 							<td>dydis:</td> 							<td>32cm</td> 						</tr> 						<tr> 							<td>Svoris:</td> 							<td>180g/td> 						</tr> 						<tr> 							<td>Medziaga:</td> 							<td>medvilne</td> 						</tr> 						<tr> 							<td>Gamintojas:</td> 							<td>Made in china</td> 						</tr> 						<tr> 							<td>Platintojas</td> 							<td>UAB kazkas</td> 						</tr>												 					</tbody> 				</table>', 180);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `goods`
--
ALTER TABLE `goods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `toys`
--
ALTER TABLE `toys`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `goods`
--
ALTER TABLE `goods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `toys`
--
ALTER TABLE `toys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
