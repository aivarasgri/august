
$(document).ready(function(){
    
    $('.parallax').parallax();
    
    $('.carousel').carousel();

    $(".dropdown-trigger").dropdown();
    
    $('.slider').slider();

    $('.sidenav').sidenav();
  
        
    
    $('.aivaro-carusele').owlCarousel({
    loop:true,
    autoplay: true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
})
    

    $('.materialboxed').materialbox();



	$('.mano-owl-carousel').owlCarousel({
	    loop:true,
	    autoplay: true,
	    items: 8,
	    autoplayTimeout: 2000,
          responsive:{
        0:{
            items:2,
        },
        600:{
            items:5,
        },
        1000:{
            items:7,
        }
    }
	});

}); 
 

    $('.carousel.carousel-slider').carousel({
    fullWidth: true,
    indicators: true
  });
    


