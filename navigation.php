<!-- NAV BAR LARGE @@@@@@@-->
<nav>
	<div class="nav-wrapper blue">
			<a href="index.php" class="brand-logo"><img  class="logo" src="images/logo1.jpg"></a>
			<a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
		 <ul class="right hide-on-med-and-down">
            <li>
            
             <div class="search-container">
                <form class="search-form search-forma" action="search.php" method="POST">
                    <input class="search-input" type="text" name="search" placeholder="Ieškoti...">
                    <button>Search</button>
                </form>
            </div>   
            
            </li>
		    <li><a href="products.php">Products</a></li>
		    <li><a href="product.php">"Random" product</a></li>
		    <!-- Dropdown Trigger -->
		    <li><a class="dropdown-trigger" href="" data-target="dropdown1">
			Apie mus<i class="material-icons right">arrow_drop_down</i></a></li>
		</ul>
  </div>
</nav>

<ul id="dropdown1" class="dropdown-content">	
  <li><a href="Aurimas.php">Aurimas</a></li>
  <li><a href="aivaras_personal.php">Aivaras</a></li>
  <li><a href="lukas_personal.php">Lukas</a></li>
  <li class="divider"></li>
</ul>
<!-- NAV BAR LARGE @@@@@@@-->
<!-- NAV BAR MOB @@@@@@@-->
<ul id="dropdown2" class="dropdown-content">	
  <li><a href="Aurimas.php">Aurimas</a></li>
  <li><a href="aivaras_personal.php">Aivaras</a></li>
  <li><a href="lukas_personal.php">Lukas</a></li>
  <li class="divider"></li>
</ul>

   <ul class="sidenav" id="mobile-demo">
    <li><a href="products.php">Products</a></li>
    <li><a href="product.php">"Random" product</a></li>   
	<li><a class="dropdown-trigger" href="4lapas.html" data-target="dropdown2">
	Apie mus<i class="material-icons right">arrow_drop_down
	</i></a></li>	
  </ul>	