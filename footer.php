<footer class="page-footer blue accent-4">
          <div class="container ">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">Footer, my dud</h5>
                <p class="grey-text text-lighten-4">Random stuff. We need sponsors and money.</p>
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Links</h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" href="https://lt-lt.facebook.com">Facebook</a></li>
                  <li><a class="grey-text text-lighten-3" href="https://twitter.com/">Twitter</a></li>
                  <li><a class="grey-text text-lighten-3" href="https://www.skype.com/en">Skype</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2018 Copyright Coding School or smth
            </div>
          </div>
        </footer>