<!DOCTYPE html>
<html>
<head>
	<title>Pistoletai Bereta</title>
	<meta charset="utf-8">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">
	<link rel="stylesheet" type="text/css" href="styles/style.css">
	<link rel="stylesheet" href="styles/owl.carousel.min.css">
	<link rel="stylesheet" href="styles/owl.theme.green.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 





</head>
<body class="background   blue lighten-2">
	<?php

	include "database_connection.php";

	include "navigation.php";

	?>

	
<div class="row">
	<div class="col s12 m12 l12">
<div class="owl-carousel mano-owl-carousel">
	<div class="item"> <img src="images/pistoletas1.jpg"></div>
  	<div class="item"> <img src="images/pistoletas2.jpg"> </div>
 	 <div class="item"> <img src="images/pistoletas3.jpg"> </div>
  	<div class="item"> <img src="images/pistoletas4.jpg"> </div>
  </div>
  <div class="col s12 m12 l12 Produktas blue center"><h2>Pistoletas</h2></div>
  <div class="col s12 m12 l12 Akcija red center"><h2>Nuolaidų lietus  su akcijos kodu(vasara, ruduo, ziema, pavasaris)</h2></div>
	</div>
</div>



<div class="container klase white">
	<div class="row">

		<div class="col s6 m6 l6">
			<h3>Bereta Elite</h3> <br>
			<h5><b>Price:  
				<?php

				if (isset($_GET['kodas'])) {



				$nuolaida = 1;

				$sql = "SELECT nuolaida FROM akcijos WHERE kodas = '" . $_GET['kodas'] . "'";


				// echo $sql;
					 // SELECT `nuolaida` FROM `akcijos` WHERE `name` == 'VASARA';
				$result = mysqli_query($conn, $sql);

				if (mysqli_num_rows($result) > 0) {

				    while($row = mysqli_fetch_assoc($result)) {

				    	$nuolaida = $row["nuolaida"];

				    }

				}

				}



				if (isset($_GET['kodas']) && $nuolaida != 1) {
					// $NuolaidosKodas = $_POST['kodas'];
					echo 78 * $nuolaida;

				} else if (isset($_GET['number'])) {

					echo 78 * $_GET['number'];
					
				} else {
					echo 78;
				}

				?>
				Eur

				</b></h5>

				<form class="v">
					<label class="red">Nuolaidos kodas: </label>
					<input type="text" name="kodas">
					<button class="waves-effect waves-light">Submit</button>
				</form>


			
			<div class="col 1">
				<form class="z" method="get">
					
					<b>Kiekis:</b><input type="number" name="number" value="1"  min="1">
					<input class="waves-effect waves-light btn mygtukas white-text" type="submit" name="submit" value="Į krepšelį" >
				</form>
					<!-- <a class="waves-effect waves-light btn">Į krepšelį</a> -->
					
				</div>
		</div>

		<div class="row" >
		<div class="col s6 m6 l6">
			<!-- <img class="materialboxed puska" src="images/Bereta.jpg" width="200"> -->
			<!-- <img class="materialboxed puska" src="images/Bereta1.jpg" width="200"> -->
			<img class="materialboxed puska" src="images/Bereta2.jpg" width="400">
			<!-- <img class="materialboxed puska" src="images/Bereta3.jpg" width="200"> -->

		</div>

<div class="col s12 m12 l12 center" ><H3>BERETA PRESENTATION</H3>

	
      <div class="video-container">
        <iframe width="853" height="480" src="https://www.youtube.com/embed/yVvPOiHGh7o" frameborder="0" allowfullscreen></iframe>
      </div>
        
<!-- <iframe width="1200" height="720"
 src="https://www.youtube.com/embed/yVvPOiHGh7o" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>">
</iframe> -->
</div>

		<div class="col s12 m12 l12">
			
		
			<p class="aprasimas">Bene populiariausias pneumatinis pistoletas, šaudantis angliarūgštės pagalba plieniniais šratukais. Šaudymas šiuo paprasto naudojimo pistoletu suteiks Jus daug džiaugsmo už ypač patrauklią kainą. Parduodamas asmenims nuo 18 metų be jokių specialių leidimų.
			</p>
		</div>
		
	</div>

</div>
	 

	<div class="row">
		<div class="col l8 offset-l2 center">
		
				<table class="lentele">
					<thead>
						<tr>
							<th>Poperties</th>	
							<th>Value</th>
							
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Type:</td>
							<td>Pistoletas</td>
							
							
						</tr>
						<tr>
							<td>Kalibras:</td>
							<td>4,5 mm BB</td>
							
						</tr>
						<tr>
							<td>Veikimo principas:</td>
							<td>CO2</td>
						</tr>
						<tr>
							<td>Dėtuvės talpa:</td>
							<td>18</td>
						</tr>
						<tr>
							<td>Energija:</td>
							<td>3 J</td>
						</tr>
						<tr>
							<td>Bendras ilgis:</td>
							<td>215 mm</td>
						</tr>
						<tr>
							<td>Vamzdžio ilgis:</td>
							<td>123 mm</td>
						</tr>
						<tr>
							<td>Svoris:</td>
							<td>686 g</td>
						</tr>												
					</tbody>
				</table> <br>
				
		</div>
	</div>
	</div>
<div class="slider">
    <ul class="slides">
      <li>
        <img src="https://i.kinja-img.com/gawker-media/image/upload/s--oJwmSGre--/c_scale,f_auto,fl_progressive,q_80,w_800/qpfddhfc6jj9efdu6gdy.jpg"> <!-- random image -->
        <div class="caption center-align">
          <h3><!-- This is our big choose! --></h3>
          <h5 class="light grey-text text-lighten-3">Viskas musu saugumui.</h5>
        </div>
      </li>
      <li>
        <img src="https://s-i.huffpost.com/gen/3141812/images/o-GUNS-facebook.jpg"> <!-- random image -->
        <div class="caption left-align">
          <h3><!-- Left Aligned Caption --></h3>
          <h5 class="light grey-text text-lighten-3">Viskas musu saugumui.</h5>
        </div>
      </li>
      <li>
        <img src="https://www.wallpaperup.com/uploads/wallpapers/2016/03/13/908552/e41089f81d22dc613a49362a8507f0ad.jpg"> <!-- random image -->
        <div class="caption right-align">
          <h3><!-- Right Aligned Captio -->n</h3>
          <h5 class="light grey-text text-lighten-3">Viskas musu saugumui.</h5>
        </div>
      </li>
      <li>
        <img src="http://www.mixtapepsd.com/wp-content/uploads/2017/03/Free-Guns-and-Bullets-Background-67.png"> <!-- random image -->
        <div class="caption center-align">
          <h3><!-- This is our big Tagline! --></h3>
          <h5 class="light grey-text text-lighten-3">Viskas musu saugumui.</h5>
        </div>
      </li>
    </ul>
  </div>
	
	<footer>
		<?php
			include "footer.php";
		?>

	</footer>
				
				
			
	<script src="scripts/owl.carousel.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
	<script type="text/javascript" src="scripts/script.js"></script>

</body>
</html>